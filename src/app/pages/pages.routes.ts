import {RouterModule, Routes } from '@angular/router';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProgressComponent } from './progress/progress.component';
import { Graficas1Component } from './graficas1/graficas1.component';
import { AccountSettingsComponent } from './account-settings/account-settings.component';
import { PromesasComponent } from './promesas/promesas.component';
import { RxjsComponent } from './rxjs/rxjs.component';
import { GatoComponent } from './gato/gato.component';
import { CulebritaComponent } from './culebrita/culebrita.component';
import { P1Component } from './p1/p1.component';
import { P2Component } from './p2/p2.component';
import { P3Component } from './p3/p3.component';
import { P4Component } from './p4/p4.component';
import { P5Component } from './p5/p5.component';
import { P6Component } from './p6/p6.component';
import { P7Component } from './p7/p7.component';
import { RecuerdaColoresComponent } from './recuerda-colores/recuerda-colores.component';
import { AhorcadoComponent } from './ahorcado/ahorcado.component';
import { GatoVersusComponent } from './gato-versus/gato-versus.component';



const pagesRoutes: Routes = [

    {path: '',
     component: PagesComponent,
        children : [
            {path: 'dashboard', component: DashboardComponent, data: { titulo: 'Dashboard'}},
            {path: 'progress', component: ProgressComponent, data: { titulo: 'Progreso'}},
            {path: 'graficas1', component: Graficas1Component , data: { titulo: 'Graficas'}},
            {path: 'promesas', component: PromesasComponent, data: { titulo: 'Promesas'}},
            {path: 'rxjs', component: RxjsComponent , data: { titulo: 'RxJs'}},
            {path: 'ahorcado', component: AhorcadoComponent , data: { titulo: 'ahorcado'}},
            {path: 'gato', component: GatoComponent , data: { titulo: 'gato'}},
            {path: 'gato-versus', component: GatoVersusComponent , data: { titulo: 'gato-versus'}},
            {path: 'recuerda-colores', component: RecuerdaColoresComponent , data: { titulo: 'recuerda-colores'}},
            {path: 'culebrita', component: CulebritaComponent , data: { titulo: 'culebrita'}},
            {path: 'p1', component: P1Component , data: { titulo: 'Pantalla 1'}},
            {path: 'p2', component: P2Component , data: { titulo: 'Pantalla 2'}},
            {path: 'p3', component: P3Component , data: { titulo: 'Pantalla 3'}},
            {path: 'p4', component: P4Component , data: { titulo: 'Pantalla 4'}},
            {path: 'p5', component: P5Component , data: { titulo: 'Pantalla 5'}},
            {path: 'p6', component: P6Component , data: { titulo: 'Pantalla 6'}},
            {path: 'p7', component: P7Component , data: { titulo: 'Pantalla 7'}},
            {path: 'account-settings', component: AccountSettingsComponent , data: { titulo: 'Ajustes'}},
            {path: '', redirectTo: '/dashboard', pathMatch: 'full'},
        ]},

];
export const  PAGES_ROUTES = RouterModule.forChild(pagesRoutes);
